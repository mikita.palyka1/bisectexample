//
//  bisectExampleApp.swift
//  bisectExample
//
//  Created by Mikita Palyka on 9.02.24.
//

import SwiftUI

@main
struct bisectExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
