//
//  ContentView.swift
//  bisectExample
//
//  Created by Mikita Palyka on 9.02.24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("Hello, world!")
            Text("first git commit")
            Text("Second good commit")
            Text("Third good commit")
            Text("Bad commit)
                 Text("last commit")
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
